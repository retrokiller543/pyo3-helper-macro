use pyo3::prelude::*;
use pyo3::{pyclass, PyErr};
use pyo3_helper_macros::{exclude, py3_bind, py3_bind_pub};
use rayon::prelude::*;
use thiserror::Error;

const THRESHOLD: usize = 1000;
const ALPHABET: &str = "abcdefghijklmnopqrstuvwxyzåäö";

#[derive(Error, Debug, PartialEq)]
pub enum CharacterParseError {
    #[error("Invalid character: {0}")]
    InvalidCharacter(char),
    #[error("Invalid index: {0}")]
    InvalidIndex(usize),
    #[error("Error: {0}")]
    Error(String),
    #[error("Unknown string type, cannot decode")]
    UnknownStringType,
}

impl From<PyErr> for CharacterParseError {
    fn from(err: PyErr) -> CharacterParseError {
        CharacterParseError::Error(err.to_string())
    }
}

impl From<CharacterParseError> for PyErr {
    fn from(err: CharacterParseError) -> PyErr {
        pyo3::exceptions::PyException::new_err(format!("{:?}", err))
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
#[pyclass]
pub enum StringType {
    Standard,
    Assymetric,
    Unknown,
}

#[py3_bind]
impl Default for StringType {
    fn default() -> Self {
        Self::Unknown
    }
}

/// String wrapper struct for impl of encoding and decoding
#[derive(Default, Debug, Clone, PartialEq, Eq)]
#[pyclass]
pub struct BaseString {
    pub data: String,
}

#[derive(Default, Debug, Clone, PartialEq, Eq)]
#[pyclass]
pub struct EncodedString {
    pub data: Vec<usize>,
    str_type: StringType,
}

#[py3_bind_pub(supported = {StringType, BaseString, EncodedString})]
impl BaseString {
    pub fn new(data: String) -> Self {
        Self { data }
    }

    pub fn encode(&self) -> Result<EncodedString, CharacterParseError> {
        let data = self.data.to_lowercase();
        let data_len = data.chars().count();

        #[cfg(feature = "debug")]
        {
            // Log the input data and its length
            dbg!(&data, data_len);
        }

        let encoded = if data_len > THRESHOLD {
            data.par_chars()
                .map(|x| {
                    #[cfg(feature = "debug")]
                    {
                        let position = crate::prelude::ALPHABET.chars().position(|y| y == x);

                        dbg!(&x, &position); // Log each character and its index (sequential branch)

                        position.ok_or(CharacterParseError::InvalidCharacter(x))
                    }
                    #[cfg(not(feature = "debug"))]
                    {
                        ALPHABET
                            .chars()
                            .position(|y| y == x)
                            .ok_or(CharacterParseError::InvalidCharacter(x))
                    }
                })
                .collect::<Result<Vec<_>, _>>()?
        } else {
            data.chars()
                .map(|x| {
                    #[cfg(feature = "debug")]
                    {
                        let position = crate::prelude::ALPHABET.chars().position(|y| y == x);

                        dbg!(&x, &position); // Log each character and its index (sequential branch)

                        position.ok_or(CharacterParseError::InvalidCharacter(x))
                    }
                    #[cfg(not(feature = "debug"))]
                    {
                        ALPHABET
                            .chars()
                            .position(|y| y == x)
                            .ok_or(CharacterParseError::InvalidCharacter(x))
                    }
                })
                .collect::<Result<Vec<_>, _>>()?
        };

        #[cfg(feature = "debug")]
        {
            // Log the final encoded data
            dbg!(&encoded);
        }

        // Return based on whether the lengths match
        if data_len == encoded.len() {
            Ok(EncodedString::new(encoded, StringType::Standard))
        } else {
            Err(CharacterParseError::UnknownStringType)
        }
    }

    pub fn encode_asym(&self) -> Result<EncodedString, CharacterParseError> {
        let data = self.data.to_lowercase();
        let data_len = data.chars().count();

        // Log the input data and its length
        #[cfg(feature = "debug")]
        dbg!(&data, data_len);

        let encoded = if data_len > THRESHOLD {
            data.par_chars()
                .map(|x| {
                    let position = ALPHABET.chars().position(|y| y == x).map(|index| index + 1); // Add 1 to avoid zero

                    #[cfg(feature = "debug")]
                    dbg!(&x, &position); // Log each character and its index

                    position.ok_or(CharacterParseError::InvalidCharacter(x))
                })
                .collect::<Result<Vec<_>, _>>()?
        } else {
            data.chars()
                .map(|x| {
                    let position = ALPHABET.chars().position(|y| y == x).map(|index| index + 1); // Add 1 to avoid zero

                    #[cfg(feature = "debug")]
                    dbg!(&x, &position); // Log each character and its index

                    position.ok_or(CharacterParseError::InvalidCharacter(x))
                })
                .collect::<Result<Vec<_>, _>>()?
        };

        // Log the final encoded data
        #[cfg(feature = "debug")]
        dbg!(&encoded);

        Ok(EncodedString::new(encoded, StringType::Assymetric))
    }

    #[exclude]
    pub fn chars(&self) -> impl Iterator<Item = char> + '_ {
        self.data.chars()
    }

    #[exclude]
    pub fn par_chars(&self) -> rayon::str::Chars<'_> {
        self.data.par_chars()
    }

    #[exclude]
    pub fn to_uppercase(self) -> Self {
        let cpy = self.clone();
        let cpy = cpy.data.to_uppercase();
        Self { data: cpy }
    }

    #[cfg_attr(feature = "pyo3-bindings", exclude)]
    pub fn to_lowercase(self) -> Self {
        let cpy = self.clone();
        let cpy = cpy.data.to_lowercase();
        Self { data: cpy }
    }

    // include more encodings here, such as base64 and binary encodings
}

#[py3_bind_pub(supported = {StringType, BaseString, EncodedString, Vec<usize>})]
impl EncodedString {
    // represents the encoded string as a vector of usize
    pub fn new(data: Vec<usize>, str_type: StringType) -> Self {
        Self { data, str_type }
    }

    // error handling falls on the user not the lib in this case, user should know what has gone wrong and handle it
    pub fn decode(&self) -> Result<BaseString, CharacterParseError> {
        match self.str_type {
            StringType::Standard => self.decode_std(),
            StringType::Assymetric => self.decode_asym(),
            _ => Err(CharacterParseError::UnknownStringType),
        }
    }

    fn decode_std(&self) -> Result<BaseString, CharacterParseError> {
        let decoded = if self.data.len() > THRESHOLD {
            self.data
                .par_iter()
                .map(|&x| {
                    ALPHABET
                        .chars()
                        .nth(x)
                        .ok_or(CharacterParseError::InvalidIndex(x))
                })
                .collect::<Result<String, _>>()?
        } else {
            self.data
                .iter()
                .map(|&x| {
                    ALPHABET
                        .chars()
                        .nth(x)
                        .ok_or(CharacterParseError::InvalidIndex(x))
                })
                .collect::<Result<String, _>>()?
        };

        Ok(BaseString::new(decoded))
    }

    fn decode_asym(&self) -> Result<BaseString, CharacterParseError> {
        let decoded = if self.data.len() > THRESHOLD {
            self.data
                .par_iter()
                .map(|&x| {
                    ALPHABET
                        .chars()
                        .nth(x - 1)
                        .ok_or(CharacterParseError::InvalidIndex(x))
                })
                .collect::<Result<String, _>>()?
        } else {
            self.data
                .iter()
                .map(|&x| {
                    ALPHABET
                        .chars()
                        .nth(x - 1)
                        .ok_or(CharacterParseError::InvalidIndex(x))
                })
                .collect::<Result<String, _>>()?
        };

        Ok(BaseString::new(decoded))
    }

    pub fn flatten(&self) -> String {
        let data = self.data.clone();
        let mut new_str = String::new();
        if self.str_type == StringType::Standard {
            for x in data {
                new_str.push_str(&x.to_string());
            }
        } else {
            // if we do not have a standard string, we treat it as a asymetric string
            // if any number but the first number is less then 10, add a 0 to the start
            for (i, x) in data.iter().enumerate() {
                if i != 0 && *x < 10 {
                    new_str.push_str(&format!("0{}", x));
                } else {
                    new_str.push_str(&x.to_string());
                }
            }
        }
        new_str
    }
}

impl EncodedString {
    pub fn iter(&self) -> impl Iterator<Item = &usize> {
        self.data.iter()
    }

    pub fn par_iter(&self) -> rayon::slice::Iter<'_, usize> {
        self.data.par_iter()
    }
}

// python magic methods

#[pymethods]
impl BaseString {
    fn __str__(&self) -> String {
        self.data.clone()
    }

    fn __repr__(&self) -> String {
        format!("BaseString({})", self.data)
    }
}

#[pymethods]
impl EncodedString {
    fn __str__(&self) -> String {
        self.flatten()
    }

    fn __repr__(&self) -> String {
        format!("EncodedString({})", self.flatten())
    }
}

#[derive(FromPyObject)]
pub enum StringOrBaseString {
    #[pyo3(annotation = "String")]
    StdString(String),
    #[pyo3(annotation = "BaseString")]
    MyBaseString(BaseString),
}

impl From<StringOrBaseString> for BaseString {
    fn from(s: StringOrBaseString) -> Self {
        match s {
            StringOrBaseString::StdString(s) => BaseString::new(s),
            StringOrBaseString::MyBaseString(s) => s,
        }
    }
}

impl From<String> for StringOrBaseString {
    fn from(s: String) -> Self {
        StringOrBaseString::StdString(s)
    }
}

#[pyfunction]
fn do_something(str: StringOrBaseString) -> EncodedString {
    let str: BaseString = str.into();
    str.encode().unwrap()
}

#[pymodule]
fn feck(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<BaseString>()?;
    m.add_class::<EncodedString>()?;
    m.add_class::<StringType>()?;
    m.add_wrapped(wrap_pyfunction!(do_something))?;
    Ok(())
}
