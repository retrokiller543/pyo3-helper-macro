#![allow(dead_code)]
#![allow(non_snake_case)]

use pyo3::prelude::PyModule;
use pyo3::{pyclass, pymodule, PyResult, Python};

use pyo3_helper_macros::*;

// TODO: Make genereics be the way to tell the macro what impls are allowed to be passed from and to pyo3

//# This is to demonstarte the usage of how we support several impl blocks even if that is not supporteed by PyO3
//# its a bit hacky but it works and is easy to use, just remeber to call the impl_pyo3 macro last in your code! otherwise it will not have anything to impl since the other impls are not constructed by this macro

#[cfg_attr(feature = "pyo3-bindings", pyclass)]
pub struct Foo {
    x: i32,
}

impl Foo {
    pub fn set(&mut self, x: i32) {
        self.x = x;
    }
}

impl From<Foo> for String {
    fn from(foo: Foo) -> Self {
        format!("Foo {{ x: {} }}", foo.x)
    }
}

#[cfg_attr(feature = "pyo3-bindings", py3_bind_pub(supported = {Vec<i32>, Foo}))] // recommend to use it like this to still get syntax highlighting and other IDE features since that tends to break with the attribute macro types
impl Foo {
    pub fn new() -> Self {
        Self { x: 0 }
    }

    pub fn add(&mut self, x: i32) {
        self.x += x;
    }

    pub fn get(&self) -> i32 {
        self.x
    }

    pub fn may_fail(&self) -> Result<i32, String> {
        if self.x == 0 {
            Err("x is zero".to_string())
        } else {
            Ok(self.x)
        }
    }
}

#[cfg_attr(feature = "pyo3-bindings", py3_bind_pub)]
impl Foo {
    pub fn div(&mut self, x: i32) {
        self.x /= x;
    }

    pub fn sub(&mut self, x: i32) {
        self.x -= x;
    }
}

pub trait FooTrait {
    fn bar(&self) -> i32;
}

#[cfg_attr(feature = "pyo3-bindings", py3_bind_pub)]
impl Foo {
    pub fn __str__(&self) -> String {
        format!("Foo {{ x: {} }}", self.x)
    }
}

// traits are weird, the macro works on it but it wont find public functions event though they are public
impl FooTrait for Foo {
    fn bar(&self) -> i32 {
        self.x
    }
}

// py module
#[cfg(feature = "pyo3-bindings")]
#[pymodule]
fn dev_lib(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<Foo>()?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let mut foo = Foo::new();
        foo.add(1);
        assert_eq!(foo.get(), 1);
        foo.sub(1);
        assert_eq!(foo.get(), 0);
        foo.div(1);
        assert_eq!(foo.get(), 0);
        foo.set(1);
        assert_eq!(foo.get(), 1);
        assert_eq!(foo.__str__(), "Foo { x: 1 }");
        assert_eq!(foo.bar(), 1);
        assert_eq!(foo.may_fail().unwrap(), 1);
    }
}
