use pyo3::prelude::*;
use pyo3_helper_macros::*;

trait Sub {
    fn sub(&mut self, x: i32);
}

#[derive(Clone)]
#[pyclass]
struct Foo {
    x: i32,
}

#[derive(Clone)]
#[pyclass]
struct Bar {
    x: i32,
}

#[py3_bind_pub(supported = {Foo, Bar})]
impl Foo {
    pub fn new() -> Self {
        Self { x: 0 }
    }

    /// This is a docstring
    /// for the add method on Foo
    pub fn add(&mut self, x: i32) {
        self.x += x;
    }

    pub fn get(&self) -> i32 {
        self.x
    }

    #[exclude]
    pub fn excluded_method(&self) -> i32 {
        self.x
    }

    #[cfg_attr(feature = "pyo3-bindings", exclude)]
    pub fn exluded_cfg_attr(&self) -> i32 {
        self.x
    }

    pub fn from_bar(_bar: Bar) -> Self {
        Self { x: _bar.x }
    }
}

#[py3_bind_pub(supported = {Foo, Bar})]
impl Bar {
    pub fn new() -> Self {
        Self { x: 0 }
    }

    pub fn add(&mut self, x: i32) {
        self.x += x;
    }

    pub fn get(&self) -> i32 {
        self.x
    }

    pub fn from_foo(_foo: Foo) -> Self {
        Self { x: _foo.x }
    }
}

#[py3_bind]
impl Sub for Foo {
    fn sub(&mut self, x: i32) {
        self.x -= x;
    }
}

#[py3_bind_pub]
impl Foo {
    /// This is a docstring
    /// for the BAR const on Foo
    const BAR: i32 = 1;
}

#[pymodule]
fn simple_example(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<Foo>()?;
    m.add_class::<Bar>()?;
    Ok(())
}

/// This is a docstring
/// for the Foo class
fn idk_docs() {
    let mut foo = Foo::new();
    foo.add(1);
    foo.sub(1);
    foo.get();
    foo.excluded_method();
    foo.exluded_cfg_attr();
    let bar = Bar::new();
    Foo::from_bar(bar);
    let foo = Foo::new();
    Bar::from_foo(foo);
}
