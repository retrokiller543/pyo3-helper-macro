use crate::is_type_ident;
use syn::Type;

// TODO expand this list
#[derive(Debug)]
pub(crate) enum PyO3SupportedType {
    String,
    StrRef,
    USize,
    U8,
    U16,
    U32,
    U64,
    U128,
    ISize,
    I8,
    I16,
    I32,
    I64,
    I128,
    F32,
    F64,
    Vec(Box<PyO3SupportedType>),
    HashMap(Box<PyO3SupportedType>),
    // ... and so on for other types supported by PyO3
}

impl PyO3SupportedType {
    pub(crate) fn from_type(ty: &syn::Type) -> Option<Self> {
        match ty {
            // Match each type to determine if it's supported, returning the corresponding enum variant
            // This is a simplified example, you'll need to handle types more precisely,
            // especially for references, generics, etc.
            _ if is_type_ident(ty, "String") => Some(PyO3SupportedType::String),
            _ if is_type_ident(ty, "str") => Some(PyO3SupportedType::StrRef),
            _ if is_type_ident(ty, "usize") => Some(PyO3SupportedType::USize),
            _ if is_type_ident(ty, "u8") => Some(PyO3SupportedType::U8),
            _ if is_type_ident(ty, "u16") => Some(PyO3SupportedType::U16),
            _ if is_type_ident(ty, "u32") => Some(PyO3SupportedType::U32),
            _ if is_type_ident(ty, "u64") => Some(PyO3SupportedType::U64),
            _ if is_type_ident(ty, "u128") => Some(PyO3SupportedType::U128),
            _ if is_type_ident(ty, "isize") => Some(PyO3SupportedType::ISize),
            _ if is_type_ident(ty, "i8") => Some(PyO3SupportedType::I8),
            _ if is_type_ident(ty, "i16") => Some(PyO3SupportedType::I16),
            _ if is_type_ident(ty, "i32") => Some(PyO3SupportedType::I32),
            _ if is_type_ident(ty, "i64") => Some(PyO3SupportedType::I64),
            _ if is_type_ident(ty, "i128") => Some(PyO3SupportedType::I128),
            _ if is_type_ident(ty, "f32") => Some(PyO3SupportedType::F32),
            _ if is_type_ident(ty, "f64") => Some(PyO3SupportedType::F64),
            _ if is_type_ident(ty, "Vec") => {
                println!("Found a Vec");
                if let Type::Path(type_path) = ty {
                    if let syn::PathArguments::AngleBracketed(args) =
                        &type_path.path.segments[0].arguments
                    {
                        if let syn::GenericArgument::Type(ty) = &args.args[0] {
                            if let Some(inner_ty) = PyO3SupportedType::from_type(ty) {
                                println!("Found a vec of something");
                                let vec = PyO3SupportedType::Vec(Box::from(inner_ty));
                                dbg!(&vec);
                                return Some(vec);
                            }
                        }
                    }
                }
                None
            }
            _ if is_type_ident(ty, "HashMap") => {
                if let Type::Path(type_path) = ty {
                    if let syn::PathArguments::AngleBracketed(args) =
                        &type_path.path.segments[0].arguments
                    {
                        if let syn::GenericArgument::Type(ty) = &args.args[1] {
                            if let Some(inner_ty) = PyO3SupportedType::from_type(ty) {
                                return Some(PyO3SupportedType::HashMap(Box::from(inner_ty)));
                            }
                        }
                    }
                }
                None
            }
            // ... and so on for other supported types
            _ => None, // If the type isn't recognized as supported, return None
        }
    }
}
