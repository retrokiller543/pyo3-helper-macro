use crate::pyo3types::PyO3SupportedType;
use proc_macro2::{Ident, TokenStream, TokenTree};
use quote::{quote, ToTokens};
use syn::parse::{Parse, ParseStream};
use syn::punctuated::Punctuated;
use syn::spanned::Spanned;
use syn::token::Comma;
use syn::{braced, parse_quote, parse_str, Attribute, FnArg, GenericArgument, ImplItem, ImplItemConst, ImplItemFn, ItemImpl, Pat, PathArguments, PathSegment, Result as SynResult, ReturnType, Token, Type, TypePath, Visibility, Meta};


pub(crate) fn get_docs(input: ImplItemFn) -> Vec<Attribute> {
    let mut new_attrs = Vec::new();

    // Iterate over attributes and duplicate doc comments
    for attr in input.attrs.iter() {
        if attr.path().is_ident("doc") {
            // Duplicate the attribute
            new_attrs.push(attr.clone());
        }
    }

    new_attrs
}

pub(crate) fn filter_items(input: Vec<ImplItem>) -> Vec<ImplItem> {
    let filtered = input.into_iter().filter_map(|item| {
        let mut included = true;
        match item.clone() {
            ImplItem::Fn(func) => {
                let attrs = &func.attrs;
                for attr in attrs {
                    // Fix for the first error: Ensuring both branches return a Result type
                    let _ = if attr.meta.path().is_ident("cfg_attr") {
                        if let Meta::List(list) = attr.clone().meta {
                            list.tokens.into_iter().for_each(|token| {
                                match token {
                                    TokenTree::Ident(ident) => {
                                        if ident.to_string() == "exclude" {
                                            included = false;
                                        }
                                    }
                                    _ => {}
                                }
                            })
                        }
                        attr.parse_nested_meta(|meta| { // this get the condition and not the attribute

                            if meta.path.is_ident("exclude") {
                                included = false;
                            }
                            Ok(()) // explicitly returning Ok(())
                        })
                    } else if attr.meta.path().is_ident("exclude") {
                        included = false;
                        Ok(()) // wrapping this in Ok(())
                    } else {
                        Ok(())
                    };
                }
            }
            _ => {}
        }
        if included {
            Some(item) // returning owned item
        } else {
            None
        }
    })
        .collect::<Vec<ImplItem>>(); // Collecting into a Vec<ImplItem>

    filtered
}

pub(crate) fn process_consts(consts: Vec<ImplItemConst>) -> Vec<TokenStream> {
    let mut const_getters = Vec::new();
    if !consts.is_empty() {
        for const_item in consts {
            let const_name = const_item.ident;
            let ty = const_item.ty;
            let const_name_str = const_name.to_string();
            let const_name_str_lower = const_name_str.to_lowercase();
            let const_name_str_lower = format!("py_{}", const_name_str_lower);
            let const_name_str_lower = Ident::new(&const_name_str_lower, const_name.span());
            let const_getter = quote! {
                #[pyo3(name = #const_name_str)]
                #[getter]
                fn #const_name_str_lower(&self) -> #ty {
                    Self::#const_name
                }
            };
            const_getters.push(const_getter);
        }
    }
    const_getters
}

pub(crate) fn is_type_ident(ty: &Type, name: &str) -> bool {
    if let Type::Path(type_path) = ty {
        type_path.path.is_ident(name)
    } else {
        false
    }
}

pub(crate) fn get_consts(input: ItemImpl) -> Vec<ImplItemConst> {
    input
        .items
        .into_iter()
        .filter_map(|item| {
            if let ImplItem::Const(constant) = item {
                return Some(constant);
            }
            None
        })
        .collect()
}

pub(crate) fn get_consts_v2(items: Vec<ImplItem>) -> Vec<ImplItemConst> {
    items
        .into_iter()
        .filter_map(|item| {
            if let ImplItem::Const(constant) = item {
                return Some(constant);
            }
            None
        })
        .collect()
}

pub(crate) fn get_pub_methods_v2(items: Vec<ImplItem>) -> Vec<ImplItemFn> {
    items
        .into_iter()
        .filter_map(|item| {
            if let ImplItem::Fn(method) = item {
                // Check if the method is public
                if let Visibility::Public(_) = method.vis {
                    return Some(method);
                }
            }
            None
        })
        .collect()
}

pub(crate) fn get_pub_methods(input: ItemImpl) -> Vec<ImplItemFn> {
    input
        .items
        .into_iter()
        .filter_map(|item| {
            if let ImplItem::Fn(method) = item {
                // Check if the method is public
                if let Visibility::Public(_) = method.vis {
                    return Some(method);
                }
            }
            None
        })
        .collect()
}

pub(crate) fn get_all_methods_v2(items: Vec<ImplItem>) -> Vec<ImplItemFn> {
    items
        .into_iter()
        .filter_map(|item| {
            if let ImplItem::Fn(method) = item {
                return Some(method);
            }
            None
        })
        .collect()
}

pub(crate) fn get_all_methods(input: ItemImpl) -> Vec<ImplItemFn> {
    input
        .items
        .into_iter()
        .filter_map(|item| {
            if let ImplItem::Fn(method) = item {
                return Some(method);
            }
            None
        })
        .collect()
}

pub(crate) fn ensure_pyo3_supported_input(
    mut method: ImplItemFn,
    supported_classes: &[Type],
) -> ImplItemFn {
    let inputs = get_inputs(&method);
    let mut new_input: Punctuated<FnArg, Comma> = Punctuated::new();

    // Check inputs for compatibility and adjust if necessary
    // This is a simplified example. You'll need to expand this logic to handle
    // all types and possibly add conversion logic.
    for input in inputs.iter() {
        if let FnArg::Typed(arg) = input {
            let is_supported_externaly = supported_classes
                .iter()
                .any(|ty| {
                    //dbg!(ty.clone().to_token_stream().to_string());
                    let tmp = &arg.ty.to_token_stream().to_string() == &ty.clone().to_token_stream().to_string();
                    //is_type_ident(&arg.ty, &ty.to_token_stream().to_string())
                    tmp
                });

            if is_supported_externaly || PyO3SupportedType::from_type(&arg.ty).is_some() {
                // If the type is supported, we can use it as-is
                new_input.push(input.clone());
            } else {
                //dbg!(&arg.ty);
                // for now we only make the input a string and tell the user to impl From<String> on their type
                let mut local_ty = arg.clone();

                local_ty.ty = Box::from(parse_str::<Type>("String").unwrap());

                new_input.push(FnArg::Typed(local_ty));
            }
        }

        if let FnArg::Receiver(_) = input {
            new_input.push(input.clone());
        }
    }

    method.sig.inputs = new_input;

    method
}

fn get_inputs(method: &ImplItemFn) -> Vec<FnArg> {
    method.sig.inputs.iter().cloned().collect()
}

fn convert_inputs_to_safe_calls(inputs: Vec<FnArg>) -> Vec<String> {
    inputs
        .iter()
        .map(|input| match input {
            FnArg::Receiver(_) => "self".to_string(),
            FnArg::Typed(pat_type) => {
                if let Pat::Ident(pat_ident) = &*pat_type.pat {
                    let pram_name = pat_ident.ident.to_string();
                    format!("{}.into()", pram_name)
                } else {
                    panic!("Unable to extract ident from input");
                }
            }
        })
        .collect()
}

fn get_output(method: &ImplItemFn) -> ReturnType {
    method.sig.output.clone()
}

pub(crate) fn wrap_function(mut method: ImplItemFn, supported_classes: &[Type]) -> ImplItemFn {
    let old_name = method.sig.ident.clone();

    let new_name = format!("py_{}", old_name);

    // Update the method identifier to the new name
    method.sig.ident = Ident::new(&new_name, old_name.span());

    let mut method = ensure_pyo3_supported_input(method, supported_classes);

    // Extracting parameters, transforming them, and preparing for the function call
    let mut call_params = convert_inputs_to_safe_calls(get_inputs(&method));

    // Constructing the new method body with the transformed parameters
    let param_list = call_params.join(", ");
    let mut new_output = method.sig.output.clone();
    let new_method_body = match method.sig.output {
        // Handling Result types
        // path to the result ident is 'type_path.path.segments[0].ident == Ident::new("Result", "Result".span())'
        ReturnType::Type(_, box Type::Path(ref type_path))
            if type_path.path.segments[0].ident == Ident::new("Result", "Result".span()) =>
        {
            // Extracting the success type from the Result<T, E>
            if let Some(PathSegment {
                arguments: PathArguments::AngleBracketed(angle_bracketed),
                ..
            }) = type_path.path.segments.last()
            {
                if let Some(GenericArgument::Type(Type::Path(TypePath {
                    path: success_type_path,
                    ..
                }))) = angle_bracketed.args.first()
                {
                    let success_type = success_type_path.segments.last().unwrap().ident.clone();

                    // construct full new return type with the success type and the pyo3::PyResult wrapper
                    // Construct the new PyResult<T> return type
                    let pyresult_type: Type = parse_quote! {
                        pyo3::PyResult<#success_type>
                    };

                    // Update the method output to the new type
                    new_output = ReturnType::Type(parse_quote!(->), Box::new(pyresult_type));

                    // Constructing the new method body
                    format!(
                        "{{ match Self::{}({}) {{
                            Ok(res) => Ok(res.into()),
                            Err(e) => Err(pyo3::exceptions::PyException::new_err(format!(\"{{:?}}\", e))),
                        }} }}",
                        old_name, param_list
                    )
                } else {
                    panic!("Unable to extract success type from Result");
                }
            } else {
                panic!("Result type is not in the expected format");
            }
        }
        // Handling non-Result types
        _ => {
            format!("{{ Self::{}({}) }}", old_name, param_list)
        }
    };

    method.sig.output = new_output;
    method.block = parse_str(new_method_body.as_str()).unwrap();

    method
}

pub(crate) fn add_attributes(mut method: ImplItemFn) -> ImplItemFn {
    let mut new_attrs: Vec<Attribute> = Vec::new();

    let orig_name = method.sig.ident.clone().to_string().replace("py_", "");

    // add the new attribute to the constructor and rename it for other methods
    if orig_name == "new" {
        new_attrs.push(parse_quote! {
            #[new]
        });
    } else {
        new_attrs.push(parse_quote! {
            #[pyo3(name = #orig_name)]
        });
    }

    // add staticmethod attribute to static methods
    let is_static_method = method.sig.inputs.iter().all(|arg| match arg {
        syn::FnArg::Receiver(_) => false, // it's some form of 'self'
        _ => true,
    });

    // Add staticmethod attribute to static methods if there is no 'self'
    if is_static_method && orig_name != "new" {
        new_attrs.push(parse_quote! {
            #[staticmethod]
        });
    }

    // apply the new attributes
    for attr in new_attrs {
        method.attrs.push(attr);
    }

    method
}
