#![feature(box_patterns)]
#![allow(unused_imports)]
#![allow(unused_variables)]
#![allow(dead_code)]
#![allow(unused_mut)]

extern crate proc_macro;

use proc_macro::TokenStream;
use std::fs::{create_dir_all, File};
use std::io::{Read, Write};

use crate::args::WrapImplArgs;
use quote::quote;
use syn::{parse_macro_input, ImplItem, ItemImpl, ItemFn};
use uuid::Uuid;

use crate::utils::*;

mod args;
mod pyo3types;
mod utils;

/// This procedural macro attribute is used
/// to wrap an implementation block with PyO3-compatible methods.
/// It processes each method in the implementation block,
/// wraps it with PyO3-compatible code, and adds necessary attributes.
/// This macro is only available when the "multiple_pymethod"
/// feature is enabled and relies on the same feature in PyO3.
///
/// Goal is to only use this macro but keep the others if you need them but discourage it heavly!
///
/// # Example
///
/// ```rust
/// # use pyo3_helper_macros::py3_bind_pub;
/// # use pyo3::prelude::*;
/// # struct MyStruct;
///
/// #[py3_bind_pub]
/// impl MyStruct {
///     pub fn my_method(&self) -> Result<i32, String> {
///         // ...
///         Ok(0)
///     }
/// }
/// ```
/// This will generate:
/// ```rust
/// # use pyo3_helper_macros::py3_bind_pub;
/// # struct MyStruct;
/// impl MyStruct {
///     pub fn my_method(&self) -> Result<i32, String> {
///         // ...
///        Ok(0)
///     }
/// }
///
/// #[pyo3::pymethods]
/// impl MyStruct {
///     #[pyo3(name = "my_method")]
///     fn py_my_method(&self, input: String) -> pyo3::PyResult<String> {
///         match Self::my_method(self, input.into()) {
///             Ok(s) => Ok(s.into()),
///             Err(e) => Err(pyo3::exceptions::PyException::new_err(format!("{:?}", e))),
///         }
///     }
/// }
/// ```
///
/// # Output
/// The macro generates a new `impl` block annotated with `#[pyo3::pymethods]`, wrapping each method
/// with the necessary PyO3-specific code and attributes.
///
/// # Features
/// - Transforms method signatures to be compatible with Python types.
/// - Handles conversion of Rust types to Python types using PyO3 conventions.
/// - Adds necessary PyO3 attributes to methods for exposure to Python.
/// - Supports custom renaming and configuration of Python-exposed methods.
///
/// # Limitations
/// - The macro is designed with certain PyO3 and Rust type conventions in mind.
///   It may not cover all complex use cases or custom type conversions.
/// - Error handling in the transformed methods should be managed carefully to ensure
///   compatibility with Python exceptions and error types.
#[cfg(feature = "multiple_pymethod")]
#[proc_macro_attribute]
pub fn py3_bind_pub(_args: TokenStream, input: TokenStream) -> TokenStream {
    let args = syn::parse_macro_input!(_args as WrapImplArgs); // keeping seperate for later support with more args
    let supported_classes = args.supported_types;
    let input = parse_macro_input!(input as ItemImpl);
    let input_orig = input.clone();
    let struct_name = &input.clone().self_ty;
    let filtered = filter_items(input_orig.items.clone());
    let methods = get_pub_methods_v2(filtered.clone());

    let mut new_methods = Vec::new();


    for method in methods {
        let mut new_method = wrap_function(method, &supported_classes);
        new_method = add_attributes(new_method);
        new_methods.push(new_method);
    }

    let consts = get_consts_v2(filtered);
    let const_getters = process_consts(consts);

    let tokens = quote! {
        #input_orig

        #[pyo3::pymethods]
        impl #struct_name {
            #![doc = "This is a generated impl for the pyo3 bindings of the struct"]
            #(#new_methods)*
            #(#const_getters)*
        }
    };

    tokens.into()
}

#[proc_macro_attribute]
pub fn py3_bind(_args: TokenStream, input: TokenStream) -> TokenStream {
    let args = syn::parse_macro_input!(_args as WrapImplArgs); // keeping seperate for later support with more args
    let supported_classes = args.supported_types;
    let input = parse_macro_input!(input as ItemImpl);
    let input_orig = input.clone();
    let struct_name = &input.clone().self_ty;
    let filtered = filter_items(input_orig.items.clone());
    let methods = get_all_methods_v2(filtered.clone());

    let mut new_methods = Vec::new();


    for method in methods {
        let mut new_method = wrap_function(method, &supported_classes);
        new_method = add_attributes(new_method);
        new_methods.push(new_method);
    }

    let consts = get_consts_v2(filtered);
    let const_getters = process_consts(consts);

    let tokens = quote! {
        #input_orig

        #[pyo3::pymethods]
        impl #struct_name {
            #![doc = "This is a generated impl for the pyo3 bindings of the struct"]
            #(#new_methods)*
        }
    };

    tokens.into()
}

#[proc_macro_attribute]
pub fn exclude(_args: TokenStream, input: TokenStream) -> TokenStream {
    input
}

/// `pyo3_bindings` Procedural Macro Attribute
///
/// This macro is part of an internal strategy to store modified `impl` blocks
/// in separate files within a directory called `python_integration`. It allows
/// managing multiple `impl` blocks and merging them later, meeting PyO3's requirements
/// without using the 'multiple_pymethod' feature.
///
/// # Usage
/// This is an internal utility macro and typically not used directly by end users.
/// It's part of the process in managing multiple Python-bound implementation blocks.
///
/// # Features
/// - Separates and stores `impl` blocks for later integration.
/// - Works as part of a strategy with `impl_pyo3` to handle multiple PyO3 `impl` blocks.
///
/// # Limitations
/// - Intended for internal use within a system utilizing both `pyo3_bindings` and `impl_pyo3`.
#[cfg(feature = "single_pymethod")]
#[proc_macro_attribute]
pub fn pyo3_bindings(_args: TokenStream, input: TokenStream) -> TokenStream {
    let args = syn::parse_macro_input!(_args as WrapImplArgs);
    let supported_classes = args.supported_types;
    let input = parse_macro_input!(input as ItemImpl);
    let input_orig = input.clone();

    let struct_name = &input.clone().self_ty;

    let methods = get_pub_methods(input);

    let mut new_methods = Vec::new();

    for method in methods {
        let mut new_method = wrap_function(method, &supported_classes);
        new_method = add_attributes(new_method);
        new_methods.push(new_method);
    }

    let tokens = quote! {
        #input_orig
    };

    let tokens_to_store = quote! {
        impl #struct_name {#(#new_methods)*}
    };

    let struct_name = quote! {
        #struct_name
    }
    .to_string();

    // Step 1: Generate a unique file name
    let unique_name = format!("py_{}{}", struct_name, Uuid::new_v4());

    // Step 2: Create the "python_integration" directory if it does not exist
    let dir_path = "python_integration";
    create_dir_all(dir_path).expect("Failed to create directory");

    // Step 3: Create a new file in the "python_integration" directory
    let path = format!("python_integration/{}", unique_name);
    let mut file = File::create(path).expect("Could not create file");

    // Step 4: Write the `tokens_to_store` to the new file
    let tokens_to_store_string = tokens_to_store.to_string();

    file.write_all(tokens_to_store_string.as_bytes())
        .expect("Could not write to file");

    tokens.into()
}

/// `impl_pyo3` Procedural Macro Attribute
///
/// This macro reads all files in the 'python_integration' directory (created by `pyo3_bindings`)
/// and merges them into a single `impl` block. This is used to circumvent limitations
/// around multiple PyO3 `impl` blocks for a single Python class or module.
///
/// # Usage
/// This macro is typically the concluding part of a process started by `pyo3_bindings`.
/// It's an internal utility macro and not usually used directly by end users.
///
/// # Features
/// - Merges multiple Rust `impl` blocks into a single PyO3-compatible `impl` block.
/// - Facilitates the organization and management of complex PyO3 bindings.
///
/// # Limitations
/// - Part of an internal strategy, not typically used in isolation.
/// - Depends on the structure and files created by `pyo3_bindings`.
#[cfg(feature = "single_pymethod")]
#[proc_macro_attribute]
pub fn impl_pyo3(_args: TokenStream, input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as ItemImpl);
    let struct_name = &input.self_ty;
    let struct_name_str = quote! {
        #struct_name
    }
    .to_string();

    // extract the methods from the input
    let methods = input.items;

    // Directory reading logic
    let paths = std::fs::read_dir("python_integration").unwrap();
    let mut all_methods = Vec::new();

    for path in paths {
        let path = path.unwrap().path();
        let path_to_delete = path.clone();
        if path.is_file()
            && path
                .file_name()
                .unwrap()
                .to_str()
                .unwrap()
                .starts_with(format!("py_{}", struct_name_str).as_str())
        {
            // Read each file and parse as ItemImpl
            let mut contents = String::new();
            File::open(path)
                .unwrap()
                .read_to_string(&mut contents)
                .unwrap();

            // Parse the contents as ItemImpl and extract methods
            let item_impl: ItemImpl = syn::parse_str(&contents).unwrap();
            for item in item_impl.items {
                if let ImplItem::Fn(method) = item {
                    all_methods.push(method);
                }
            }
        } else {
            println!("Skipping file: {:?}", path);
        }

        match std::fs::remove_file(path_to_delete.clone()) {
            Ok(_) => println!("Removed file: {:?}", path_to_delete),
            Err(_) => println!("Could not remove file: {:?}", path_to_delete),
        };
    }

    let paths = std::fs::read_dir("python_integration").unwrap();
    if paths.count() == 0 {
        std::fs::remove_dir("python_integration").unwrap();
    } else {
        println!("Directory not empty, file missed perhaps?");
    }

    // Combine all methods into a single implementation block
    let tokens = quote! {
        // TODO: Must rewrite this to just make the impl and not a new module
        mod python_integration {
            use pyo3::prelude::*;
            use super::*;
            #[pymethods]
            impl #struct_name {
                #(#all_methods)*
                #(#methods)*
            }
        }
    };

    tokens.into()
}
