use proc_macro2::Ident;
use std::collections::HashMap;
use syn::parse::{Parse, ParseStream};
use syn::punctuated::Punctuated;
use syn::{braced, Result as SynResult, Token, Type};

pub(crate) struct WrapImplArgs {
    pub(crate) supported_types: Vec<Type>,
    pub(crate) convert_types: HashMap<Type, Type>,
}

impl Parse for WrapImplArgs {
    fn parse(input: ParseStream) -> SynResult<Self> {
        let mut supported_types = Vec::new();
        let mut convert_types = HashMap::new();

        if !input.is_empty() {
            // change back to while when more args are added
            let ident: Ident = input.parse()?;
            let _eq: Token![=] = input.parse()?;

            if ident == "supported" {
                let content;
                let _braces = braced!(content in input);
                // Since the content is more complex, parse it as a Rust type
                let types: Punctuated<Type, Token![,]> =
                    content.parse_terminated(Type::parse, Token![,])?;
                supported_types.extend(types);
                //break; // Stop after parsing `supported`
            } else {
                return Err(input.error("Expected 'supported'"));
            }
        }

        Ok(WrapImplArgs {
            supported_types,
            convert_types,
        })
    }
}
