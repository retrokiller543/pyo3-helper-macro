# Helper Macro For PyO3

This is a crate to provide helper macros for the [PyO3](https://pyo3.rs) crate. Currently, it only supports wrapping impl blocks with the necessary boilerplate to make them work with PyO3 without having to rewrite functions. 

To make this crate not use hacky workarounds to make it work you must use the feature `multiple-pymethod` for the PyO3 crate and same for this crate. This will enable you to use more than one impl block for a type and activate the `wrap_impl` macro in this crate.
If this is not used you will need to use the `pyo3_bindings` macro to generate and store the wrapped functions in a file until the `impl_pyo3` macro is called that will then generate one single impl block with all the functions in it.

*Note that the names of the macros will change in future releases*

## Future Plans

- [ ] Add support for args where user can specify the supported custom types
- [ ] Add an optional arg where user can specify what we should change some types from and to

See the docs for information on how to use the macros.